Natalia Zdrowak, gr. lab. 8, II IS

Programowanie w Java

Harmonogram projektu

Temat: Gra logiczna dwuosobowa statki

Zasady gry: Każdy z graczy ma do dyspozycji 7 różnej wielkości okrętów, których ułożenie wybiera z 10 dostępnych w grze układów (rozmiar planszy: 10x10). Gracze nie znają układu przeciwnika i strzelają do siebie w kolejnych ruchach. Wygrywa gracz, który jako pierwszy zatopi wszystkie statki przeciwnika.

1 Okienka programu:

- okno logowania
- menu
- okno z wyborem układu i właściwa gra
- okno statystyk

2 Zapis i odczyt plików:

- odczyt planszy
- zapis statystyk
- odczyt statystyk
- zapis 10 dostępnych układów
- odczyt 10 dostępnych układów

3 Współbieżność:

- główny wątek dla GUI 
- obsługa sieci na oddzielnym wątku
- obsługa myszy
- dodanie tła gry
 
4 Bazy danych:

- statystyki
- dostępne układy w grze
- imiona graczy
- wysyłanie do statystyk imienia gracza, który wygrał

5 Komunikacja sieciowa:

- utworzenie klienta do połączenia z serwerem
- utworzenia serwera do którego będzie łączyć się klient
- stworzenie połączenia dwóch użytkowników w sieci
- użycie ServerSocket
- czat

6 Zaproponowane przez studenta:

-  rozmieszczenie statków (nie mogą się stykać, ani zakrzywiać)
-  naprzemienne strzały
-  zaznaczenie obszaru wokół zatopionego statku, w którym nie może znajdować się żaden     z  inny
-  trafienie statku oznaczone krzyżykiem (X)
